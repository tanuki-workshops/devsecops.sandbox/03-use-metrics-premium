const http = require('http')
const port = process.env.PORT || 8080

// this is a lion 🦁 🍊
const requestHandler = (request, response) => {

  let params = request.url.split("/").filter(item => item !== "")

  if(params.length==0) {
    response.writeHead(200, {"Content-Type": "text/html; charset=utf-8"});
    response.end(`
      <!doctype html>
      <html>
        <head>
          <meta charset="utf-8">
          <meta http-equiv="x-ua-compatible" content="ie=edge">
          <title>Hello World!</title>
          <meta name="description" content="">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style>
              .container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
              .title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
              .subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
              .links { padding-top: 15px; }
          </style>
        </head>
        <body>
          <section class="container">
            <div>
              <h1 class="title">
                👋 Hello World 🌍 
              </h1>   
              <h2 class="subtitle">
                Made with 💚 by 🤖🌻
              </h2>    
              <h2 class="subtitle">
                Hosted by OpenFaaS - Docker 🐳 powered
              </h2>      
            </div>
          </section>
          <script>

          </script>
        </body>
      </html>  
    `)
  } else {
    response.writeHead(200, {"Content-Type": "application/json; charset=utf-8"});
    response.end(JSON.stringify({
      message: "hello world", params: params
    }))
  }
}

// this is tiger 🐯
const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    console.log('😡 something bad happened', err)
    process.exit(1)
  }

  console.log(`🌍 server is listening on ${port}`)
})
